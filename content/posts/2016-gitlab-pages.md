Title: Pelican on GitLab Pages!
Date: 2016-03-25
Category: GitLab
Tags: pelican, gitlab
Slug: pelican-on-gitlab-pages

This site is hopefully hosted on GitLab Pages, including correct encodings for – and [äöüß]!

The source code of this site is at <https://gitlab.com/pages/pelican>.

Learn about GitLab Pages at <https://pages.gitlab.io>.
